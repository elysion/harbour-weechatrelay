# WeeCRApp

WeeCRApp, or WeeChat Relay App, is a WeeChat remote GUI client for Sailfish. It
uses the WeeChat relay protocol to talk to a WeeChat instance running on some
server and displays the open channels and chat history. It is not an IRC client
in itself and cannot function without an accessible WeeChat instance.

Note: The client is in the stages of early development, so features are scarce
and revisions may not compile.

## Possible future features

* Connect to WeeChat on a server, optionally with SSL (done)
* Store SSL certificates for later (done)
* Debug window (done)
* Display all open buffers with possibility to change buffer
* Display buffer list and nick list with some appropriate GUI
* Autoconnect on start
* Automatic reconnect
* OS notifications for highlights and other interesting stuff
* Automatic URL recognition
* IRC shortcuts (nick completion, aliases etc.)
* Image upload from device camera and pasting link to channel

## Licence

WeeCRApp is licenced with the MIT Expat licence. See the LICENCE file for more
details.

## Thanks

The development of WeeCRApp is graciously sponsored by
[Vincit Oy](http://www.vincit.fi/).