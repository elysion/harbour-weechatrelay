/*
 * © Mikko Ahlroth 2014
 * WeeCRApp is open source software. For licensing information, please check
 * the LICENCE file.
 */

import QtQuick 2.0
import Sailfish.Silica 1.0
import "pages"
import "js/connection.js" as C

import harbour.weechatrelay.connectionhandler 1.0

ApplicationWindow
{
    initialPage: Component { ConnectionList { } }
    cover: Qt.resolvedUrl("cover/DefaultCover.qml")

    Component.onCompleted: {
        C.init(connectionHandler, pageStack);

        // Connect signals to UI logic
        connectionHandler.connected.connect(C.onConnected);
        connectionHandler.disconnected.connect(C.onDisconnected);
        connectionHandler.displayDebugData.connect(C.onDisplayDebugData);
        connectionHandler.newEvent.connect(C.newEvent);
        connectionHandler.sslError.connect(C.onSslError);
    }

    ConnectionHandler {
        id: connectionHandler
    }
}
