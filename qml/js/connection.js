.pragma library
.import harbour.weechatrelay.connectionhandler 1.0 as CH
.import harbour.weechatrelay.qsslcertificateinfo 1.0 as QSCI
.import "storage.js" as S
.import "debug.js" as D
.import "eventqueue.js" as EQ
.import "buffers.js" as B
.import "config.js" as C

/*
 * © Mikko Ahlroth 2014
 * WeeCRApp is open source software. For licensing information, please check
 * the LICENCE file.
 */

/*
 * This file contains the main UI logic in the handling of a relay connection.
 * It will create the necessary views and pass messages to them.
 */

// State variables
var connected = false;
var connection = null; // A Connection object
var handler = null; // The underlying ConnectionHandler
var ps = null; // The PageStack

// Views
var debugViewPage = null;
var bufferListPage = null;
var sslVerifyDialog = null;


// Set initial variables before using any other functions
function init(connectionHandler, pageStack) {
    handler = connectionHandler;
    ps = pageStack;

    // Register to receive all send events to actually send them to the backend
    // This is done so we can avoid cyclic imports
    EQ.addHandler("__weecrapp_send", function(str) {
        send(str);
    });

    // Wait for init reply to let system know it's ready to send
    EQ.addHandler("init", function(data) {
        // Check the received version, it must match the minimum supported
        var version = data[0]['value'];
        if (!C.SUPPORTED_WEECHAT_VERSIONS.test(version)) {
            D.d("This version of WeeChat is not supported.");
            disconnect();
        }
        else {
            D.d("Welcome to WeeChat " + version);
            EQ.handleEvent("__weecrapp_ready");
        }
    }, true);

    B.init(ps);
}


// Signal handlers for incoming data from connection
function newEvent(id, data) {
    D.c("Event received: " + id);
    D.c(JSON.stringify(data));
    EQ.handleEvent(id, data);
}

function onDisplayDebugData(str) {
    D.c(str);
}

function onConnected() {
    connected = true;
    debugViewPage.connected();
    D.d("Connected");
    EQ.handleEvent("__weecrapp_connected");
}

function onDisconnected() {
    connected = false;
    debugViewPage.disconnected();
    D.d("Disconnected");
    EQ.handleEvent("__weecrapp_disconnected");
}

function onSslError(errorStrings,
                    issuerInfo,
                    startDate,
                    expiryDate,
                    digest) {
    D.d("SSL verification error when connecting!");

    sslVerifyDialog = ps.push("../pages/SslVerifyDialog.qml",
                              {
                                  "errorList": errorStrings,
                                  "issuer": issuerInfo,
                                  "startDate": startDate,
                                  "expiryDate": expiryDate,
                                  "digest": digest
                              });
}


// Public API

function connect(connObj) {
    debugViewPage = ps.replace("../pages/BufferView.qml");
    bufferListPage = ps.pushAttached("../pages/BufferList.qml", {
                                         bufferViewPage: debugViewPage
                                     });
    B.setDebugPage(debugViewPage);
    D.d("Connecting...");

    var connType = (connObj.type === "ssl")
                    ? CH.ConnectionHandler.SSL
                    : CH.ConnectionHandler.NONE;

    // Use any stored certificates
    if ('cert' in connObj.options
            && 'digest' in connObj.options.cert
            && connObj.options.cert.digest.length > 0) {

        var qmlSpec = "import harbour.weechatrelay.qsslcertificateinfo 1.0; QSslCertificateInfo {}";
        var info = Qt.createQmlObject(qmlSpec, handler, '');
        info.effectiveDate = connObj.options.cert.effectiveDate;
        info.expiryDate = connObj.options.cert.expiryDate;
        info.digest = connObj.options.cert.digest;
        handler.acceptCertificate(info);
    }

    handler.connect(CH.ConnectionHandler.WEECHAT,
                    connType,
                    connObj.host,
                    connObj.port,
                    connObj.password);
    connection = new S.Connection(connObj);
}

function disconnect() {
    connected = false;
    handler.disconnect();
}

function reconnect() {
    D.d("Reconnecting...");
    handler.reconnect();
}

// Reconnect, accepting the previously failed certificate
function reconnectWithFailed() {
    D.d("Reconnecting...");
    handler.reconnectWithFailed();
}

// Store the failed certificate information so that it will be accepted automatically
// on the next connection
function storeFailedCertificate() {
    var cert = handler.getFailedCertificate();

    var storedInfo = {
        effectiveDate: cert.effectiveDate,
        expiryDate: cert.expiryDate,
        digest: cert.digest
    };

    connection.options['cert'] = storedInfo;
    S.storeConnection(S.connect(), connection);
}

function clearConnection() {
    connection = null;
    connected = false;
    handler.clearData();
    ps.replace("../pages/ConnectionList.qml");
}

// Send a command to the WeeChat server
function send(str) {
    D.c("Sending command: " + str);
    handler.send(str);
}


// Private API

