/*
 * © Mikko Ahlroth 2014
 * WeeCRApp is open source software. For licensing information, please check
 * the LICENCE file.
 */

/*
 *
 */

.pragma library

.import QtQuick 2.0 as QQ

.import "eventqueue.js" as EQ
.import "moment.js" as M
.import "debug.js" as D
.import "weechatcolors.js" as WC

function Buffer(pointer, number, name, title) {
    this.pointer = pointer;
    this.number = number;
    this.name = name;
    this.title = title;
    this.visible = false;

    // Amount of unread lines in this buffer
    this.unreadCount = 0;

    // A listmodel where we put the messages of this buffer
    this.textList = Qt.createQmlObject('import QtQuick 2.0; ListModel {}',
                                       _parentPage);

    // Add line to this buffer
    this.add = function(moment, prefix, str) {
        this.textList.insert(0, {
                                   time: moment.format('HH:mm:ss'),
                                   prefix: prefix,
                                   str: str
                                });

        if (!this.active) {
            ++this.unreadCount;
        }
    };

    // This buffer has become visible to the user
    this.activate = function() {
        this.active = true;
        this.unreadCount = 0;
    }

    // This buffer will be hidden from the user
    this.deactivate = function() {
        this.active = false;
    }

    // Send input into this buffer
    this.input = function(str) {
        EQ.command('input 0x' + this.pointer + ' ' + str);
    };
}

// Dict of buffers, pointer as key, for fast referencing
var _buffers = {};

// List of all buffers for easy iteration, will be sorted ascending
// according to buffer number whenever requested
var _buffersList = [];

// Page that will own all of our textlistmodels
var _parentPage = null;


function init() {
    EQ.addHandler("__weecrapp_ready", connected);
    EQ.addHandler("_buffer_line_added", lineAdded);
}

// Set the given page to be used for the core buffer
// This is so that we can reuse the debug page as the
// WeeChat core buffer
function setDebugPage(page) {
    _parentPage = page;

    var pointer = '0';
    var buffer = new Buffer(pointer, 1, 'weechat', 'Debug/core');
    _buffers[pointer] = buffer;
    _buffersList.push(buffer);
    D.setDebugBuffer(buffer);
    page.changeBuffer(buffer);
}

function getBuffer(i) {
    if (_buffers.hasOwnProperty(i)) {
        return _buffers[i];
    }

    return null;
}

// Return bufferlist sorted by buffer number
function getBuffersByNumber() {
    return _buffersList;
}


function connected() {
    D.d("Fetching all buffers...");
    EQ.command("hdata buffer:gui_buffers(*) full_name,short_name,number,type,title", bufferListData);
    EQ.command("sync");
}

function disconnected() {

}



function bufferListData(data) {
    data = data[0];
    var buffers = data.objectSets;

    for (var i = 0; i < buffers.length; ++i) {
        var pointer = buffers[i]['__path'][0];
        var number = buffers[i].number;
        var name = buffers[i].short_name;
        var title = buffers[i].title;
        var buffer =  null;

        if (buffers[i].type !== 0) {
            // Currently only sync IRC buffers
            continue;
        }

        // If this buffer is the core, merge it to the debug buffer
        if (_parentPage !== null && number === 1 && buffers[i].full_name === 'core.weechat') {
            _buffers[pointer] = _buffers['0'];
            buffer = _buffers['0'];
            buffer.pointer = pointer;
            buffer.name = name;
            buffer.title = title;
            buffer.number = number;
        }
        else {
            buffer = new Buffer(pointer, number, name, title);
            _buffers[pointer] = buffer;
            _buffersList.push(buffer);
        }
    }

    _sortBuffersList();
}

// A new line was added to a buffer
function lineAdded(data) {
    data = data[0].objectSets[0];

    var buffer = data.buffer;
    var date = M.moment(data.date);
    var prefix = WC.codedText2StyledText(WC.parseString(data.prefix));
    var message = WC.codedText2StyledText(WC.parseString(data.message));
    _buffers[buffer].add(date, prefix, message);
}

function _sortBuffersList() {
    _buffersList.sort(function (a, b) {
        return a.number - b.number;
    });
}
