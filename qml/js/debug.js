/*
 * © Mikko Ahlroth 2014
 * WeeCRApp is open source software. For licensing information, please check
 * the LICENCE file.
 */

.pragma library

.import "moment.js" as M

// Buffer to push messages to
var _buffer = null;

// Write a line to the debug view (will go to console if debug view isn't open)
function debug(str) {
    if (_buffer !== undefined && _buffer !== null) {
        _buffer.add(M.moment(), 'DEBUG', str);
    }
    else {
        console.log(str);
    }
}

function setDebugBuffer(o) {
    _buffer = o;
}


function d(str) {
    debug(str);
}

// Go straight to console
function c(str) {
    console.log(str);
}

function e(o) {
    enumerate(o);
}

function enumerate(o) {
    c("Enumerating object");
    c("---");

    var keys = Object.keys(o);

    for (var i = 0; i < keys.length; ++i) {
        var val = o[keys[i]];

        if (typeof val === 'undefined') {
            c(keys[i] + ": undefined");
        }
        else if (val === null) {
            c(keys[i] + ": null");
        }
        else {
            c(keys[i] + ": " + val);
        }
    }

    c("");
}
