/*
 * © Mikko Ahlroth 2014
 * WeeCRApp is open source software. For licensing information, please check
 * the LICENCE file.
 */

/*
 * Unique IDs will be used to match responses with the right
 * callbacks.
 */

.pragma library

var _counter = 0;
var _prefix = "msg_";

function get() {
    return _prefix + _counter++;
}
