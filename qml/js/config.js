.pragma library

/*
 * © Mikko Ahlroth 2014
 * WeeCRApp is open source software. For licensing information, please check
 * the LICENCE file.
 */

/*
 * This file contains the configuration settings of the app (both hardcoded
 * and user-settable) and the API to change them. Connection-specific settings
 * are handled in AddConnection.qml.
 */

.import "storage.js" as S



/*
 * Constants that cannot be changed
 */

// Regex to match weechat version strings
var SUPPORTED_WEECHAT_VERSIONS = /^(?:0\.4)|(?:1\.0)/;


/*
 * All settings and their defaults
 */

var _s = {
    timestamp_format: 'HH:mm:ss'
};

var _init = false;

// Load all settings on first config access
if (!_init) {
    _loadConfig();
}


function set(key, value) {
    _s[key] = value;

    var db = S.connect();
    S.storeSetting(db, key, value);
}

function get(key) {
    return _s[key];
}



function _loadConfig() {
    var db = S.connect();

    for (var key in _s) {
        var val = S.readSetting(db, key, _s[key]);
    }
}
