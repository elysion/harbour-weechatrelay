/*
 * © Mikko Ahlroth 2014
 * WeeCRApp is open source software. For licensing information, please check
 * the LICENCE file.
 */

import QtQuick 2.0
import Sailfish.Silica 1.0
import "../js/storage.js" as S
import "../js/connection.js" as C
import "."

Page {
    id: connectionListPage

    onVisibleChanged: connectionList.updateList();

    SilicaListView {
        id: connectionList
        model: ListModel { id: connectionModel }
        anchors.fill: parent

        header: PageHeader {
            width: connectionList.width
            title: "Connections"
        }

        delegate: ListItem {
            id: listItem
            contentHeight: Theme.itemSizeLarge
            menu: contextMenu
            ListView.onRemove: animateRemoval(listItem);

            function remove() {
                remorseAction("Removing connection",
                              function() {
                                  S.deleteConnection(S.connect(),
                                                     connectionModel.get(index).id);
                                  connectionModel.remove(index);
                              });
            }

            Column {
                anchors.verticalCenter: parent.verticalCenter

                Label {
                    text: name
                    color: listItem.highlighted ? Theme.highlightColor
                                                : Theme.primaryColor;
                    anchors {
                        left: parent.left
                        right: parent.right
                        margins: Theme.paddingLarge
                    }
                }

                Label {
                    text: host + ":" + port
                    color: listItem.highlighted ? Theme.secondaryHighlightColor
                                                : Theme.secondaryColor;
                    anchors {
                        left: parent.left
                        right: parent.right
                        margins: Theme.paddingLarge
                    }
                }
            }

            Component {
                id: contextMenu
                ContextMenu {
                    MenuItem {
                        text: "Edit"
                        onClicked: pageStack.push(Qt.resolvedUrl("AddConnection.qml"),
                                                  {
                                                      "connList": connectionList,
                                                      "oldId": connectionModel.get(index).id
                                                  });
                    }

                    MenuItem {
                        text: "Remove"
                        onClicked: remove();
                    }
                }
            }

            onClicked: {
                C.connect(connectionModel.get(index));
            }
        }

        VerticalScrollDecorator { flickable: connectionList }

        Component.onCompleted: updateList();


        function updateList() {
            connectionModel.clear();

            var db = S.connect();
            var connList = S.readAllConnections(db);

            for (var i = 0; i < connList.length; ++i) {
                connectionModel.append(connList[i]);
            }
        }


        PullDownMenu {
            MenuItem {
                text: "About"
                onClicked: pageStack.push(Qt.resolvedUrl("."));
            }

            MenuItem {
                text: "Settings"
                onClicked: pageStack.push(Qt.resolvedUrl("."));
            }

            MenuItem {
                text: "Add connection"
                onClicked: pageStack.push(Qt.resolvedUrl("AddConnection.qml"), { "connList": connectionList });
            }
        }

        PushUpMenu {
            MenuItem {
                text: "Go to top"
                onClicked: connectionList.scrollToTop();
            }
        }
    }
}


