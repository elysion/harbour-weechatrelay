/*
 * © Mikko Ahlroth 2014
 * WeeCRApp is open source software. For licensing information, please check
 * the LICENCE file.
 */

import QtQuick 2.0
import Sailfish.Silica 1.0

import "../js/buffers.js" as B

Page {
    id: bufferListPage

    property BufferView bufferViewPage : null;

    onVisibleChanged: bufferList.updateList();

    SilicaListView {
        id: bufferList
        model: ListModel { id: bufferListModel }
        anchors.fill: parent

        function updateList() {
            bufferListModel.clear();

            var buffers = B.getBuffersByNumber();

            for (var i = 0; i < buffers.length; ++i) {
                bufferListModel.append({
                                           pointer: buffers[i].pointer,
                                           number: buffers[i].number,
                                           name: buffers[i].name,
                                           unreadCount: buffers[i].unreadCount
                                       });
            }
        }

        header: PageHeader {
            width: bufferList.width
            title: "Buffers"
        }

        delegate: ListItem {
            id: listItem
            contentHeight: Theme.itemSizeSmall
            menu: contextMenu
            ListView.onRemove: animateRemoval(listItem);

            function close() {
                remorseAction("Closing buffer",
                              function() {
                                  // TODO Close buffer forrealz
                                  console.log("Closed " + name + "!");
                              });
            }

            Row {
                anchors.verticalCenter: parent.verticalCenter
                spacing: Theme.paddingLarge

                anchors {
                    left: parent.left
                    right: parent.right
                }

                Label {
                    text: number
                    color: listItem.highlighted ? Theme.secondaryHighlightColor
                                                : Theme.secondaryColor;

                    width: parent.width / 7
                    horizontalAlignment: Qt.AlignRight
                }

                Label {
                    text: name
                    color: listItem.highlighted ? Theme.highlightColor
                                                : Theme.primaryColor;
                }

                Label {
                    text: unreadCount
                    color: listItem.highlighted ? Theme.highlightColor
                                                : Theme.primaryColor;
                }
            }

            Component {
                id: contextMenu
                ContextMenu {
                    MenuItem {
                        text: "Close"
                        onClicked: close();
                    }
                }
            }

            onClicked: {
                var buffer = B.getBuffer(pointer);
                bufferViewPage.changeBuffer(buffer);
                pageStack.navigateBack(PageStackAction.Animated);
            }
        }

        VerticalScrollDecorator { flickable: connectionList }


        PullDownMenu {
            MenuItem {
                text: "???"
            }
        }

        PushUpMenu {
            MenuItem {
                text: "Go to top"
                onClicked: bufferList.scrollToTop();
            }
        }
    }
}
