import QtQuick 2.0
import Sailfish.Silica 1.0
import "."

import "../js/debug.js" as D
import "../js/connection.js" as C

Page {
    id: bufferViewPage

    property bool isConnected: false;
    property string pointer: "";
    property int number: 0;
    property string name: "";
    property var buffer: null;

    function setTitle(str) {
        topic.title = str;
    }

    function connected() {
        isConnected = true;
    }

    function disconnected() {
        isConnected = false;
    }

    function changeBuffer(newBuffer) {
        if (buffer !== null) {
            buffer.deactivate();
        }

        // Replace current list model with new list
        D.c('Changing buffer to ' + newBuffer.name);
        buffer = newBuffer;
        topic.title = newBuffer.title;
        textList.model = newBuffer.textList;
        buffer.activate();
    }

    SilicaFlickable {
        id: mainFlickable
        anchors.fill: parent

        PageHeader {
            id: topic
            title: "Debug"
            anchors.top: parent.top
        }

        TextListComponent {
            id: textList

            anchors.top: topic.bottom
            anchors.bottom: inputRow.top
            width: parent.width
            clip: true
        }

        Row {
            id: inputRow

            width: parent.width
            anchors.bottom: parent.bottom

            TextField {
                id: inputField
                width: parent.width
                anchors.bottom: parent.bottom

                placeholderText: "Type here"

                EnterKey.enabled: text.length > 0
                EnterKey.iconSource: "image://theme/icon-m-enter-accept"
                EnterKey.onClicked: {
                    if (buffer !== null) {
                        buffer.input(inputField.text);
                    }

                    inputField.text = "";
                }
            }
        }

        PushUpMenu {
            MenuItem {
                text: isConnected? "Disconnect" : "Close";
                onClicked: {
                    if (isConnected) {
                        C.disconnect();
                        isConnected = false;
                    }
                    else {
                        C.clearConnection();
                    }
                }
            }
        }
    }
}
