/*
 * © Mikko Ahlroth 2014
 * WeeCRApp is open source software. For licensing information, please check
 * the LICENCE file.
 */

import QtQuick 2.0
import Sailfish.Silica 1.0
import "../js/storage.js" as Storage

Dialog {
    id: addConnectionDialog

    canAccept: false

    property SilicaListView connList;
    property int oldId: -1;

    // Custom properties for saving connection info when modifying
    property var oldinfo: null;

    function updateFields(infodict) {
        nameField.text = infodict.name;
        hostField.text = infodict.host;
        portField.text = infodict.port;
        passwordField.text = "FAKE PASS";

        switch (infodict.type) {
        case "plain":
            securityField.currentIndex = 0;
            break;

        case "ssl":
            securityField.currentIndex = 1;
        }

        oldinfo = infodict;
    }

    function saveFields() {
        var id = null;
        var pass = passwordField.text;
        var type = "";

        if (oldinfo !== null) {
            id = oldinfo.id;

            if (passwordField.text === "FAKE PASS") {
                pass = oldinfo.password;
            }
        }

        switch (securityField.currentIndex) {
        case 0:
            type = "plain";
            break;

        case 1:
            type = "ssl";
        }

        return {
            "id": id,
            "name": nameField.text,
            "host": hostField.text,
            "port": portField.text,
            "password": pass,
            "type": type,
            "options": {}
        };
    }

    function setCanAccept() {
        addConnectionDialog.canAccept = (nameField.text.length !== 0
            && hostField.text.length !== 0
            && portField.text.length !== 0);
    }

    Component.onCompleted: {
        // Load old data if available
        if (oldId === -1) return;

        var db = Storage.connect();
        var infodict = Storage.readConnection(db, oldId);
        updateFields(infodict);
    }

    onAccepted: {
        var infodict = saveFields();
        var connection = new Storage.Connection(infodict);
        var db = Storage.connect();
        Storage.storeConnection(db, connection);
        connList.updateList();
    }


    SilicaFlickable {
        id: addConnectionFlickable
        anchors.fill: parent
        contentHeight: addConnectionColumn.height

        VerticalScrollDecorator { flickable: addConnectionFlickable }

        Column {
            id: addConnectionColumn
            width: addConnectionDialog.width
            spacing: Theme.paddingLarge

            DialogHeader {
                title: "Add connection"
                acceptText: "Save"
                cancelText: "Cancel"
            }

            TextField {
                id: nameField
                placeholderText: "Connection name"
                width: parent.width
                onTextChanged: setCanAccept();

                EnterKey.enabled: text.length > 0
                EnterKey.iconSource: "image://theme/icon-m-enter-next"
                EnterKey.onClicked: hostField.focus = true
            }

            Row {
                spacing: Theme.paddingSmall
                width: parent.width

                TextField {
                    id: hostField
                    width: parent.width * 0.75
                    placeholderText: "Hostname"
                    inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText
                    onTextChanged: setCanAccept();

                    EnterKey.enabled: text.length > 0
                    EnterKey.iconSource: "image://theme/icon-m-enter-next"
                    EnterKey.onClicked: portField.focus = true
                }

                TextField {
                    id: portField
                    width: parent.width * 0.25
                    placeholderText: "Port"
                    inputMethodHints: Qt.ImhDigitsOnly

                    validator: IntValidator {
                        bottom: 1
                        top: 65535
                    }

                    onTextChanged: setCanAccept();

                    EnterKey.enabled: text.length > 0
                    EnterKey.iconSource: "image://theme/icon-m-enter-next"
                    EnterKey.onClicked: passwordField.focus = true
                }
            }

            TextField {
                id: passwordField
                placeholderText: "Password"
                width: parent.width
                echoMode: TextInput.Password
                onTextChanged: setCanAccept();

                EnterKey.enabled: text.length > 0
                EnterKey.iconSource: "image://theme/icon-m-enter-next"
                EnterKey.onClicked: securityField.focus = true
            }

            ComboBox {
                id: securityField
                label: "Security"

                menu: ContextMenu {
                    MenuItem {
                        text: "None"
                    }

                    MenuItem {
                        text: "SSL"
                    }
                }
            }
        }
    }
}


