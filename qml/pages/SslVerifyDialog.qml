import QtQuick 2.0
import Sailfish.Silica 1.0

import "../js/utils.js" as U
import "../js/moment.js" as M
import "../js/connection.js" as C

Dialog {
    id: sslVerifyDialog

    property string errorList;
    property string issuer;
    property var startDate;
    property var expiryDate;
    property string startDateText;
    property string expiryDateText;
    property string digest;

    canAccept: acceptSwitch.checked;

    onAccepted: {
        if (saveSwitch.checked) {
            C.storeFailedCertificate();
        }

        C.reconnectWithFailed();
    }

    SilicaFlickable {
        id: sslVerifyFlickable

        anchors.fill: parent
        contentHeight: sslVerifyColumn.height + Theme.paddingLarge

        VerticalScrollDecorator { flickable: sslVerifyFlickable }

        Component.onCompleted: {
            startDate = M.moment(startDate);
            expiryDate = M.moment(expiryDate);

            startDateText = startDate.format();
            expiryDateText = expiryDate.format();
        }

        Column {
            id: sslVerifyColumn

            spacing: Theme.paddingMedium

            DialogHeader {
                id: connectHeader
                title: "SSL Verification"
                acceptText: "Connect"
                cancelText: "Disconnect"
                width: sslVerifyFlickable.width
            }

            Column {
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: Theme.paddingLarge
                anchors.rightMargin: Theme.paddingLarge

                spacing: Theme.paddingMedium

                Label {
                    font.pixelSize: Theme.fontSizeLarge
                    text: "SSL verification failed"
                    wrapMode: Text.Wrap
                    color: Theme.highlightColor
                    width: parent.width
                }

                Label {
                    text: "The following errors occurred while attempting to connect "
                          + "to the remote host:"
                    wrapMode: Text.Wrap
                    color: Theme.highlightColor
                    width: parent.width
                }

                Label {
                    text: errorList
                    wrapMode: Text.Wrap
                    color: Theme.highlightColor
                    font.weight: Font.Bold
                    width: parent.width
                }

                Label {
                    text: "This can mean that someone is trying to eavesdrop on your "
                          + "connection or that the certificate is invalid or self-signed. "
                          + "Please read through the errors and the information provided "
                          + "below. Only continue connecting if you are certain of the "
                          + "identity of the remote host. You can only accept the dialog "
                          + "once you have scrolled fully down."
                    wrapMode: Text.Wrap
                    color: Theme.highlightColor
                    width: parent.width
                }

                PageHeader {
                    title: "Certificate information"
                }

                Label {
                    text: U.colored(Theme.secondaryHighlightColor, "Issuer:<br>") + issuer
                    wrapMode: Text.Wrap
                    color: Theme.highlightColor
                    width: parent.width
                    textFormat: Text.StyledText
                }

                Label {
                    text: U.colored(Theme.secondaryHighlightColor, "Valid from:<br>") + startDateText
                    wrapMode: Text.Wrap
                    color: Theme.highlightColor
                    width: parent.width
                    textFormat: Text.StyledText
                }

                Label {
                    text: U.colored(Theme.secondaryHighlightColor, "Valid to:<br>") + expiryDateText
                    wrapMode: Text.Wrap
                    color: Theme.highlightColor
                    width: parent.width
                    textFormat: Text.StyledText
                }

                Label {
                    text: U.colored(Theme.secondaryHighlightColor, "SHA-256 digest:<br>") + digest
                    wrapMode: Text.Wrap
                    color: Theme.highlightColor
                    width: parent.width
                    textFormat: Text.StyledText
                }

                Label { text: " " }

                TextSwitch {
                    id: acceptSwitch
                    text: "I have verified this certificate is correct and wish to continue"
                }

                TextSwitch {
                    id: saveSwitch
                    text: "Automatically accept this certificate in the future"
                }
            }
        }
    }
}
