# The name of your app.
# NOTICE: name defined in TARGET has a corresponding QML filename.
#         If name defined in TARGET is changed, following needs to be
#         done to match new name:
#         - corresponding QML filename must be changed
#         - desktop icon filename must be changed
#         - desktop filename must be changed
#         - icon definition filename in desktop file must be changed
TARGET = harbour-weechatrelay

CONFIG += sailfishapp c++11

SOURCES += src/harbour-weechatrelay.cpp \
    src/relayconnection.cpp \
    src/sslrelayconnection.cpp \
    src/connectionhandler.cpp \
    src/weechatprotocolhandler.cpp \
    src/protocolhandler.cpp \
    src/qsslcertificateinfo.cpp \
    src/weechat/dynamictypeexception.cpp

OTHER_FILES += qml/harbour-weechatrelay.qml \
    rpm/harbour-weechatrelay.spec \
    rpm/harbour-weechatrelay.yaml \
    harbour-weechatrelay.desktop \
    qml/js/storage.js \
    qml/pages/ConnectionList.qml \
    qml/cover/DefaultCover.qml \
    qml/pages/AddConnection.qml \
    qml/pages/DebugView.qml \
    qml/pages/TextListComponent.qml \
    qml/pages/SslVerifyDialog.qml \
    qml/js/utils.js \
    qml/js/moment.js \
    qml/js/connection.js \
    qml/js/debug.js \
    qml/js/buffers.js \
    qml/js/eventqueue.js \
    qml/js/uniqid.js \
    qml/pages/BufferView.qml \
    qml/js/config.js \
    qml/pages/BufferList.qml \
    qml/js/weechatcolors.js

HEADERS += \
    src/relayconnection.h \
    src/sslrelayconnection.h \
    src/connectionhandler.h \
    src/protocolhandler.h \
    src/weechatprotocolhandler.h \
    src/connectresolver.h \
    src/qsslcertificateinfo.h \
    src/weechat/dynamictypeexception.h

QT += network

# This is needed on the dev machine to get Qt Creator intellisense working correctly
# It should have no adverse effect onother machines, though
INCLUDEPATH += /Users/nicd/SailfishOS/mersdk/targets/SailfishOS-armv7hl/usr/include/c++/4.6.4

