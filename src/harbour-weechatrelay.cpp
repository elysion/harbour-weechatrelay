/*
 * © Mikko Ahlroth 2014
 * WeeCRApp is open source software. For licensing information, please check
 * the LICENCE file.
 */

#ifdef QT_QML_DEBUG
#include <QtQuick>
#endif

#include <sailfishapp.h>
#include <QCoreApplication>
#include <QGuiApplication>
#include <QtQml>

#include "connectionhandler.h"
#include "qsslcertificateinfo.h"

int main(int argc, char* argv[])
{
    // SailfishApp::main() will display "qml/template.qml", if you need more
    // control over initialization, you can use:
    //
    //   - SailfishApp::application(int, char *[]) to get the QGuiApplication *
    //   - SailfishApp::createView() to get a new QQuickView * instance
    //   - SailfishApp::pathTo(QString) to get a QUrl to a resource file
    //
    // To display the view, call "show()" (will show fullscreen on device).

    QCoreApplication::setApplicationName("WeeCRApp");
    QCoreApplication::setOrganizationName("Nytsoi Inc.");
    QCoreApplication::setOrganizationDomain("nytsoi.net");


    // Register custom types to be accessible from QML
    qmlRegisterType<ConnectionHandler>("harbour.weechatrelay.connectionhandler", 1, 0, "ConnectionHandler");
    qmlRegisterType<QSslCertificateInfo>("harbour.weechatrelay.qsslcertificateinfo", 1, 0, "QSslCertificateInfo");

    return SailfishApp::main(argc, argv);
}
