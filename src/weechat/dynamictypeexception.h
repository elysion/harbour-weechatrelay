/*
 * © Mikko Ahlroth 2014
 * WeeCRApp is open source software. For licensing information, please check
 * the LICENCE file.
 */

#ifndef DYNAMICTYPEEXCEPTION_H
#define DYNAMICTYPEEXCEPTION_H

#include <stdexcept>
#include <string>

class DynamicTypeException : public std::runtime_error
{
public:
    explicit DynamicTypeException(const std::string& __arg);
};

#endif // DYNAMICTYPEEXCEPTION_H
