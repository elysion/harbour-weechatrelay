/*
 * © Mikko Ahlroth 2014
 * WeeCRApp is open source software. For licensing information, please check
 * the LICENCE file.
 */

#include "dynamictypeexception.h"

DynamicTypeException::DynamicTypeException(const std::string &__arg):
    std::runtime_error(__arg)
{
}
