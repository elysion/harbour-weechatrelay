/*
 * © Mikko Ahlroth 2014
 * WeeCRApp is open source software. For licensing information, please check
 * the LICENCE file.
 */

#ifndef SSLRELAYCONNECTION_H
#define SSLRELAYCONNECTION_H

#include <QSslSocket>
#include <QString>
#include <QList>
#include <QSslError>

#include "relayconnection.h"
#include "connectionhandler.h"

class SSLRelayConnection : public RelayConnection
{
    Q_OBJECT
public:
    explicit SSLRelayConnection(QSslSocket* socket, QObject* parent = 0);

    void setHandler(ConnectionHandler* handler);

signals:
    void newData(QByteArray* data);

public slots:
    void connect(QString host, uint port);

    void socketSslErrors(const QList<QSslError>& errors);

private:
    QSslSocket* sslSocket;
    ConnectionHandler* handler;

};

#endif // SSLRELAYCONNECTION_H
