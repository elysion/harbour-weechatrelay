/*
 * © Mikko Ahlroth 2014
 * WeeCRApp is open source software. For licensing information, please check
 * the LICENCE file.
 */

#include "qsslcertificateinfo.h"

QSslCertificateInfo::QSslCertificateInfo(QSslCertificate cert, QObject* parent) :
    QObject(parent)
{
    effectiveDate = cert.effectiveDate();
    expiryDate = cert.expiryDate();
    digest = cert.digest(QCryptographicHash::Sha3_512).toHex();
}

QSslCertificateInfo::QSslCertificateInfo():
    QObject(0)
{}

QDateTime QSslCertificateInfo::getEffectiveDate() const
{
    return effectiveDate;
}

QDateTime QSslCertificateInfo::getExpiryDate() const
{
    return expiryDate;
}

QString QSslCertificateInfo::getDigest() const
{
    return digest;
}

void QSslCertificateInfo::setEffectiveDate(QDateTime effectiveDate)
{
    this->effectiveDate = effectiveDate;
}

void QSslCertificateInfo::setExpiryDate(QDateTime expiryDate)
{
    this->expiryDate = expiryDate;
}

void QSslCertificateInfo::setDigest(QString digest)
{
    this->digest = digest;
}
