/*
 * This snippet helps when connecting to overloaded signals using the new
 * connect syntax.
 *
 * See http://stackoverflow.com/a/16795664
 */

#ifndef CONNECTRESOLVER_H
#define CONNECTRESOLVER_H

template<typename... Args> struct SELECT {
    template<typename C, typename R>
    static constexpr auto OVERLOAD_OF( R (C::*pmf)(Args...) ) -> decltype(pmf) {
        return pmf;
    }
};

#endif // CONNECTRESOLVER_H
