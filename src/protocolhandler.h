/*
 * © Mikko Ahlroth 2014
 * WeeCRApp is open source software. For licensing information, please check
 * the LICENCE file.
 */

#ifndef PROTOCOLHANDLER_H
#define PROTOCOLHANDLER_H

#include <QObject>
#include <QByteArray>
#include <QString>
#include <QDataStream>

#include "relayconnection.h"

class ProtocolHandler : public QObject
{
    Q_OBJECT
public:
    explicit ProtocolHandler(QObject* parent = 0);
    virtual ~ProtocolHandler() {}

    // Initalize protocol connection and login with password
    virtual void initialize(QString password) = 0;

signals:
    void debugData(QString hexString);
    void newEvent(QString id, QVariant data);

    // ProtocolHandler wants to send data to the associated network stream
    void sendData(QByteArray data);

public slots:
    virtual void handleNewData(RelayConnection* connection) = 0;
    virtual void sendDebugData(QString string) = 0;


protected:
    void emitData(QString data);
    QDataStream* read(RelayConnection* connection, quint64 bytes);

    // How many bytes we need to be available until we execute the next read
    qint64 bytesNeeded;
};

#endif // PROTOCOLHANDLER_H
