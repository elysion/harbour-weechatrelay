/*
 * © Mikko Ahlroth 2014
 * WeeCRApp is open source software. For licensing information, please check
 * the LICENCE file.
 */

#ifndef QSSLCERTIFICATEINFO_H
#define QSSLCERTIFICATEINFO_H

#include <QObject>
#include <QSslCertificate>
#include <QDateTime>
#include <QString>

// This class is used as a struct to pass certificate information to the UI
class QSslCertificateInfo : public QObject
{
    Q_OBJECT
public:
    explicit QSslCertificateInfo(QSslCertificate cert, QObject* parent = 0);
    QSslCertificateInfo();

    Q_PROPERTY(QDateTime effectiveDate READ getEffectiveDate WRITE setEffectiveDate)
    Q_PROPERTY(QDateTime expiryDate READ getExpiryDate WRITE setExpiryDate)
    Q_PROPERTY(QString digest READ getDigest WRITE setDigest)

    QDateTime getEffectiveDate() const;
    QDateTime getExpiryDate() const;
    QString getDigest() const;

    void setEffectiveDate(QDateTime effectiveDate);
    void setExpiryDate(QDateTime expiryDate);
    void setDigest(QString digest);

private:
    QDateTime effectiveDate;
    QDateTime expiryDate;
    QString digest;

};

#endif // QSSLCERTIFICATEINFO_H
